USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease` ()
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;

SELECT COUNT(*) INTO numberOfAlbums FROM albums;
SELECT COUNT(*) INTO numberOfBands FROM bands;

SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) +1;
SET randomBandId = FLOOR(RAND() * numberOfBands +1);

 IF(randomAlbumId, randomBandId) NOT IN (SELECT * FROM albumreleases) THEN
INSERT INTO albumreleases(Bands_Id, Albums_Id)
VALUES (randomBandId, randomAlbumId);
 END IF;

END$$

DELIMITER ;

