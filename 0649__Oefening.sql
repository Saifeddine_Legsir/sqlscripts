USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder2` ()
BEGIN
DECLARE rand INT DEFAULT 0;

DECLARE EXIT HANDLER FOR SQLSTATE '45002' 
BEGIN
SELECT 'State 45002 opgevangen. Geen probleem.' Message;
END;

DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN
RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
END;

SET rand = FLOOR(RAND()*3)+1;
IF rand = 1 THEN
signal sqlstate '45001';
ELSEIF rand = 2 THEN
signal sqlstate '45002';
ELSE 
signal sqlstate '45003';
END IF;
END$$

DELIMITER ;

