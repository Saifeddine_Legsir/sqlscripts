USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration` (IN albumId INT, OUT Duration SMALLINT UNSIGNED)
BEGIN
DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
DECLARE errHand INT DEFAULT 0;
DECLARE totalDuration INT DEFAULT 0;
DECLARE songLength CURSOR FOR SELECT liedjes.Lengte FROM liedjes WHERE Albums_Id = albumId;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET errHand = 1;
OPEN songLength;


get_Song : LOOP
FETCH songLength INTO songDuration;
IF errHand = 1 THEN
LEAVE get_Song;
END IF;



SET totalDuration = totalDuration + songDuration;
-- SET totalDuration = Duration;

END LOOP get_Song;
 
SELECT totalDuration;
CLOSE songLength;
END$$

DELIMITER ;

