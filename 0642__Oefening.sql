USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN givenDate DATE, INOUT numberCleaned INT)
BEGIN
DECLARE totalCount INT DEFAULT 0;
START TRANSACTION;
SELECT COUNT(*)
INTO totalCount FROM lidmaatschappen;
DELETE FROM lidmaatschappen WHERE Einddatum < givenDate;
SET numberCleaned = totalCount - (SELECT COUNT(*) FROM lidmaatschappen);
COMMIT;				
END$$

DELIMITER ;

