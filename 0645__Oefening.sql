USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleaseWithSuccess` (OUT succes BOOL)
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;

SET numberOfAlbums = (SELECT COUNT(*) FROM albums);
SET numberOfBands = (SELECT COUNT(*) FROM bands);

SET randomAlbumId = FLOOR(RAND() * numberOfAlbums);
SET randomBandId = FLOOR(RAND() * numberOfBands);
 IF NOT EXISTS (SELECT Albums_Id FROM albumreleases WHERE Albums_Id = randomAlbumId) AND  NOT EXISTS (SELECT Bands_Id FROM albumreleases WHERE Bands_Id = randomBandId) THEN
INSERT INTO albumreleases(Bands_Id, Albums_Id)
VALUES (randomBandId, randomAlbumId);
SET succes = 1;
ELSE
SET succes =0;
END IF;
END$$

DELIMITER ;

