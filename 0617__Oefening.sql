USE ModernWays;
CREATE VIEW AuteursBoekenRatings AS
SELECT CONCAT(Voornaam, ' ',Familienaam) AS "Auteur" ,Titel, Rating FROM Boeken 
INNER JOIN Publicaties ON Boeken.Id = Publicaties.Boeken_id
INNER JOIN Personen ON Publicaties.Personen_Id = Personen.Id
INNER JOIN GemiddeldeRatings ON Boeken.Id = GemiddeldeRatings.Boeken_Id




