USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
start transaction;
INSERT INTO albums(Titel) VALUES (titel);
INSERT INTO albumreleases(Bands_Id, Albums_Id) VALUES (bands_Id, last_insert_id());
commit;
END$$

DELIMITER ;

