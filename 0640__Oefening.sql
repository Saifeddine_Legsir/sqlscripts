USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetLiedjes` (IN keyword VARCHAR(50))
BEGIN
SELECT Titel FROM liedjes
WHERE Titel LIKE CONCAT('%',keyword,'%');
END$$

DELIMITER ;