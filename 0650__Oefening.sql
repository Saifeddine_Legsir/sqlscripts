USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
DECLARE randomBandid INT DEFAULT 0;
DECLARE randomAlbumid INT DEFAULT 0;
DECLARE count INT DEFAULT 0;
DECLARE rand INT DEFAULT 0;

DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
ROLLBACK;
SELECT 'Nieuwe releases konden niet worden toegevoegd.' Message;
END;
START TRANSACTION;
SET rand = FLOOR(RAND()*3)+1;
REPEAT
SET randomBandid = FLOOR(RAND()*1000)+1;
SET randomAlbumid = FLOOR(RAND()*1000)+1;
INSERT INTO albumreleases(Bands_Id, Albums_Id)
VALUES
(randomBandid, randomAlbumid);
SET count = count + 1;
IF count = 2 THEN 
IF rand = 3 THEN
signal sqlstate '45000';
END IF;
END IF;
UNTIL count = 3
END REPEAT;
COMMIT;
END$$

DELIMITER ;

