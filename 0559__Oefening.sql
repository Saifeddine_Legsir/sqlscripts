USE ModernWays;
ALTER TABLE Liedjes
ADD COLUMN Artiesten_id INT,
ADD CONSTRAINT fk_Liedjes_Artiesten FOREIGN KEY (Artiesten_id)
REFERENCES Artiesten(id);