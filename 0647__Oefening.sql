USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
DECLARE counter INT DEFAULT 0;
DECLARE success bool;
mock_loop : LOOP
IF counter = extraReleases THEN
LEAVE mock_loop;
ELSE
CALL MockAlbumReleaseWithSuccess(success);
IF success = 1 THEN
SET counter = counter +1;

END IF;
END IF;
END LOOP;

END$$

DELIMITER ;

