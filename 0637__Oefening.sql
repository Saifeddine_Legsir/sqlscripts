-- een nieuw tabel "Personen" wordt aangemaakt met kolommen "Voornaam" en "Familienaam"
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);


-- gegevens (voornamen en familienamen) worden uit de tabel Boeken overgezet naar de tabel Personen
-- dit met een subquery

insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
   
-- Er wordt een primary key en wat andere kolommen toegevoegd aan de tabel Personen

alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
   
-- In de table Boeken wordt er een kolom aangemaakt die de foregn key zal bevatten
-- Normaal zou ik een foutmelding moeten krijgen, maar die kreeg ik niet

alter table Boeken add Personen_Id int not null;

-- hier worden de Id waarden gelijkgesteld
-- dit doen we door een set clause te gebruiken. We stellen de Id waarden gelijk waar zowel de voornaam als familienaam overeenkomen
-- met de voornaam en familienaam van het andere tabel

SET SQL_SAFE_UPDATES =0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES =1;

-- We verwijderen de kolom Voornaam en Familienaam uit het tabel Boeken want we hebben het niet meer nodig
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
-- We voegen hier een constraint op kolom Personen_Id
-- Nu is het ook een echte foreign key
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);








