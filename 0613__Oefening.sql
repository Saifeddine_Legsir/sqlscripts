USE ModernWays;
CREATE VIEW AuteursBoeken AS
SELECT CONCAT(Voornaam, ' ',Familienaam) AS "Auteur" , Titel FROM Boeken
INNER JOIN Publicaties ON Boeken.Id = Publicaties.Boeken_id
INNER JOIN Personen ON Publicaties.Personen_Id = Personen.Id